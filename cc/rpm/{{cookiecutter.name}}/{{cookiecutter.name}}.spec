Name:		{{cookiecutter.name}}
Version:	%{ci_version}
Release:	%{ci_release}%{?dist}
Summary:	{{cookiecutter.summary}}

License:	Apache-2.0
URL:		https://gitlab.com/kev-net/rpms/{{cookiecutter.name}}/
Source0:	

BuildRequires:	
Requires:	

%description
{{cookiecutter.description}}

%prep
%autosetup


%build
%configure
%make_build


%install
%make_install


%files
%defattr(644,root,root)
%license LICENSE
%doc README.md


%changelog
* {% now 'local', '%a %b %d %Y' %} Kevin L. Mitchell <klmitch@mit.edu>
- Initial creation of the RPM
